import React, {useEffect, useState} from "react"
import Table from '../components/Table'
import Form from '../components/Form'
import data from '../data/data-temp'
import {useHistory, useLocation} from "react-router-dom";
import queryString from 'query-string';
import {wasItFound} from "../common/Search";

const tableHeaders = [
    {id: 1, title: 'نام تغییر دهنده'},
    {id: 2, title: 'تاریخ'},
    {id: 3, title: 'نام آگهی'},
    {id: 4, title: 'فیلد'},
    {id: 5, title: 'مقدار قدیمی'},
    {id: 6, title: 'مقدار جدید'},
];

const Home = () => {
    let history = useHistory();
    let location = useLocation();
    const [searchFields, setSearchFields] = useState({
        name: '',
        date: '',
        title: '',
        field: ''
    });
    const [bookmarkData, setBookmarkData] = useState([]);
    const [tableData, setTableData] = useState(data);
    useEffect(() => {
        (Object.keys(queryString.parse(location.search)).length > 0) && setSearchFields(queryString.parse(location.search));
        setBookmarkData(getBookmarks());
    }, []);
    useEffect(() => {
        history.push(`/?name=${searchFields.name}&date=${searchFields.date}&title=${searchFields.title}&field=${searchFields.field}`);
        let realData=[];
        if(searchFields.date && searchFields.date !== ''){
            const founded = wasItFound(searchFields.date);
            founded !== -1 && realData.push(founded);
        }else{
            realData = data
        }
        const newData = realData.filter(c => {
            let valid = true;
            for (const property in searchFields) {
                if (searchFields[property] && searchFields[property] !== '') {
                    if(property !== 'date'){
                        valid = valid && (c[`${property}`].toLowerCase().includes(searchFields[property].toLowerCase()))
                    }
                }

            }
            return valid
        });
        newData.sort(function (a, b) {
            if (a.date < b.date) {
                return -1;
            }
            if (a.date > b.date) {
                return 1;
            }
            return 0;
        })
        setTableData(newData);
    }, [searchFields]);

    const handleSearchInput = (event) => {
        setSearchFields({
            ...searchFields,
            [event.target.name]: event.target.value
        })
    };
    const handleBookmark = (event, id) => {
        const stars = getBookmarks();
        let data = [];
        if (event.target.checked) {
            stars && (data = stars);
            data.push(id);
            setBookmarks(data)
        } else {
            data = stars.filter(c => c !== id);
            setBookmarks(data)
        }
        setBookmarkData(data)
    };
    const setBookmarks = (data) => {
        localStorage.setItem('bookmarked', JSON.stringify(data))
    };
    const getBookmarks = () => {
        return JSON.parse(localStorage.getItem('bookmarked'))
    };
    const handleDateChange = (day) => {
        setSearchFields({
            ...searchFields,
            date: day
        })
    };

    return <div className="container">
        <Form fields={searchFields} handleInputChange={handleSearchInput} handleDateChange={handleDateChange}/>
        <Table headers={tableHeaders} data={tableData} handleBookmark={handleBookmark} bookmarks={bookmarkData}/>
    </div>
};
export default Home