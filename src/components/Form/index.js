import React, {useState,Fragment} from "react"
import DayPicker from 'react-day-picker'
import 'react-day-picker/lib/style.css'
import './style.scss'
import moment from 'moment'
import ClickAwayListener from 'react-click-away-listener';

const Table = (props) => {
    const {fields} = props;
    const [selectedDate, setSelectedDate] = useState(null);
    const [showDatepicker, setShowDatepicker] = useState(false);
    const handleDayClick = (day) => {
        setSelectedDate(day);
        props.handleDateChange(moment(day).format('YYYY-MM-DD'));
        setShowDatepicker(false);
    };
    const handleClickAway = () => {
        setShowDatepicker(false);
    };
    return <form className="search" autoComplete="off">
        <div className='search__control'>
            <input className='search__input' type="text" id='name' name='name' value={fields.name}
                   onChange={(e) => props.handleInputChange(e)}/>
            <label className={`search__label ${fields.name && fields.name.length > 0 ? 'search__label-active' : ''}`}
                   htmlFor="name">نام تغییر دهنده</label>
        </div>
            <ClickAwayListener  onClickAway={handleClickAway}>
            <Fragment>
                <input className='search__input' type="text" id='date' name='date'
                       value={fields.date}
                       onClick={() => setShowDatepicker(true)}
                       onChange={(e) => props.handleInputChange(e)}
                />
                <label
                    className={`search__label ${fields.date && fields.date.length > 0 ? 'search__label-active' : ''}`}
                    htmlFor="date">تاریخ</label>
                <DayPicker className={`${showDatepicker ? 'show' : 'hide'}`} selectedDays={selectedDate}
                           onDayClick={handleDayClick}/>
            </Fragment>
            </ClickAwayListener>
        <div className='search__control'>
            <input className='search__input' type="text" id='title' name='title' value={fields.title}
                   onChange={(e) => props.handleInputChange(e)}/>
            <label className={`search__label ${fields.title && fields.title.length > 0 ? 'search__label-active' : ''}`}
                   htmlFor="title">نام آگهی</label>
        </div>
        <div className='search__control'>
            <input className='search__input' type="text" id='field' name='field' value={fields.field}
                   onChange={(e) => props.handleInputChange(e)}/>
            <label className={`search__label ${fields.field && fields.field.length > 0 ? 'search__label-active' : ''}`}
                   htmlFor="field">فیلد</label>
        </div>
    </form>
};

export default Table