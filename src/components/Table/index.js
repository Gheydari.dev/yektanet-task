import React from "react";
import {Star} from '../Icons'
import './style.scss'
const Table = (props) => {
    const {data,headers,bookmarks} = props;
    return <div className="twrapper">
        <table className='table'>
            <thead className='table__header'>
            <tr>
                {headers.map((item, index) => {
                    return <th key={index}>{item.title}</th>
                })}
            </tr>
            </thead>
            <tbody className='table__body'>
            {data.map((item, index) => {
                return <tr key={index}>
                    <td className="text-right">
                        <label className='table__icon' htmlFor={`star${index}`}>
                            <input onClick={(e)=>props.handleBookmark(e,item.id)} checked={(bookmarks && bookmarks.filter(c => c === item.id).length >0) } type="checkbox" name="staring" id={`star${index}`}/>
                            <i><Star/></i>
                        </label>
                        {item.name}</td>
                    <td>{item.date}</td>
                    <td className="table__body-narrow">{item.title}</td>
                    <td>{item.field}</td>
                    <td>{item.old_value}</td>
                    <td>{item.new_value}</td>
                </tr>
            })}
            </tbody>
        </table>
    </div>
};

export default Table