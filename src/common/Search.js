import data from '../data/data'
function bst(arr, val) {
    let high = arr.length - 1;
    let low = 0;
    let mid = 0;
    while (low < high) {
        mid = Math.floor((high + low) / 2);
        // check middle
        if (arr[mid].date === val) {
            return arr[mid]
        } else if (val > arr[mid].date) {
            // right hand
            low = mid + 1
        }else{
            // left hand
            high = mid -1
        }
    }
    return -1
}
let sorted = data.sort(function (a, b) {
    if (a.date < b.date) {
        return -1;
    }
    if (a.date > b.date) {
        return 1;
    }
    return 0;
});

export const wasItFound = (value) => bst(sorted,value);


